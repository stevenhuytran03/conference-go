import requests
import json
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


# PEXELS_API_KEY = "563492ad6f917000010000012d810bcfe1f34de78a98d40d45dd9d6e"


# def get_city_photo(city, state):
#     query = {"query": f"{city} {state}"}
#     url = "https://api.pexels.com/v1/search"
#     auth = {"Authorization": PEXELS_API_KEY}
#     response = requests.get(url, headers=auth, params=query)  # .get requires a url. (from where are you trying to receive data) headers needed for authhorization from api keys. params is the query or input a user would type in
#     content = json.loads(response.content)  # need a content variable(can be named anything but conventionally content. needed for json.loads) # needs CLar
#     # for photo in content["photos"]:
#     #     print(photo["url"])
#     # print(content["photos"][1]["url"])
#     try:
#         return {"picture_url": content["photos"][0]["src"]["original"]}
#     except (KeyError, IndexError):
#         return {"picture_url": None}


# get_city_photo("Los Angeles", "CA")


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page":1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    gps_params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY
    }
    gps_url = "http://api.openweathermap.org/geo/1.0/direct"
    gps_response = requests.get(gps_url, params=gps_params)
    gps_content = json.loads(gps_response.content)
    lat = gps_content[0]["lat"]
    lon = gps_content[0]["lon"]

    weather_param = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_response = requests.get(weather_url, params=weather_param)
    weather_content = json.loads(weather_response.content)

    try:
        return {
            "expect": weather_content["weather"][0]["description"],
            "temp": weather_content["main"]["temp"],
            "feels like": weather_content["main"]["feels_like"],
            "wind speed": weather_content["wind"]["speed"],
            "gust speed": weather_content["wind"]["gust"],
            "humidity": weather_content["main"]["humidity"],
        }
    except (TypeError, IndexError):
        return {"weather": None}

    # try:
    #     return {
    #         "weather": {
    #             "temp": weather_content["main"]["temp"],
    #             "expect": weather_content["weather"][0]["description"],
    #             "wind": weather_content["wind"]["speed"],
    #             "gust": weather_content["wind"]["gust"],
    #         }
    #     }
    # except (TypeError, IndexError):
    #     return {"weather": None}












# def get_photo(city, state):
#     query = {
#         "query": f"{city} {state}",
#         "size": "large",

#     }
#     headers = {"Authorization": PEXELS_API_KEY}
#     response = requests.get("https://api.pexels.com/v1/search", params=query, headers=headers)
#     content = json.loads(response.content)

# get_photo("San Francisco", "CA")
