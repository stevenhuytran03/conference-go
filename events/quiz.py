# def sum_summables(dictionary):
#     total = 0
#     for thing, value in dictionary.items():
#         if isinstance(value, int) is True:
#             total += value #
#         # elif isinstance(value, float) is True:
#         #     total += value
#         # elif value.isnumeric() is True and isinstance(value, str) is True:
#         #     total += value
#         # else:
#         #     pass

#     # loop over the things, get the ones you want

#     return total

# Solution

def sum_summables(dictionary):
    total = 0
    for value in dictionary.values():
        if isinstance(value, (int, float)):
            total += value
        elif isinstance(value, str):
            if value.isnumeric():
                total += float(value)

    return total


d = {
    "dog": 1,
    "number": "three",  # this is the its not counting
    "size": "2",
    "heavy": True,  # this is being counted as 1
    "weight": 3.4,
}


print(sum_summables(d))



########### solution works below
# def only_items_with(items, filters):
#     result = []

#     for item in items:
#         keep = False
#         for key, value in filters.items():
#             if item.get(key) == value:
#                 keep = True
#                 break
#         if keep:
#             result.append(item)

#     # loop over the things, get the things you want

#     return result


# items = [
#     {"color":"blue", "size":"small"},
#     {"color":"red", "size":"small"},
#     {"color":"purple", "size":"medium"},
#     {"color":"green", "size":"large"},
# ]
# filters = {
#     "color": "blue",
#     "size": "medium"
# }


# print(only_items_with(items, filters))
